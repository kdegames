In this file:

* About kdegames
* Debugging
* Additional games
* More Info

About kdegames
--------------
This is a compilation of more than 20 various casual desktop games.

* bovo
    Five-in-a-row Board Game.
    
* cmake
    CMake modules needed to build KDE Games.

* doc
    XML based documentation for the programs.

* katomic
    Build complex atoms with a minimal amount of moves.

* kbackgammon
    Play backgammon against a local human player, via a game server or
    against GNU Backgammon (not included)

* kbattleship
    Sink battleship of your opponents, with built-in game server.

* kblackbox
    Find the balls hidden in the black box by shooting laser beams!

* kbounce
    Claim areas and don't get disturbed.

* kgoldrunner
    A game of action and puzzle-solving.

* kiriki
    Yahtzee-like Dice Game.

* kjumpingcube
    A tactical game for number-crunchers.

* klines
    Place 5 equal pieces together, but wait, there are 3 new ones.

* kmahjongg
    A tile laying patience.

* kmines
    The classical mine sweeper.

* kolf
    A mini golf game.

* konquest
    Conquer the planets of your enemy.

* kpat
    Several patience card games.

* kreversi
    The old reversi board game, also known as Othello.

* ksame
    Collect pieces of the same color.

* kshisen
    Patience game where you take away all pieces.

* kspaceduel
    Two player game with shooting spaceships flying around a sun.

* ksquares
    Connect the dots to create squares.
    
* ksudoku
    Sudoku game and more.

* ktuberling
    Kids game: make your own potato (NO french fries!)

* kwin4
    Place 4 pieces in a row.

* libkdegames
    KDE game library used by many of these programs, contains also images
    of card decks.

* libkmahjongg
    Library used for loading and rendering of Mahjongg tilesets.

* lskat
    Lieutnant skat.

Debugging
---------
You can use -DCMAKE_BUILD_TYPE=debugfull with the cmake command script, if you
want to have debug code in your KDE apps and libs. We recommend to do this, 
since this is alpha software and this makes debugging things a whole lot 
easier.

Additional Games
----------------
There are additional games and game-related applications and libraries that are 
not yet mature enough to be included in the KDE Games module or are being
developed by other teams. Most of them can be found on the KDE playground,
but some are hosted elsewhere, including some game servers.
For a complete list, see the INDEX file in playground/games.

More Info
---------
Please direct any bug reports to our bug list by visiting
http://bugs.kde.org.

Discussions related to the KDE Games module or the games in playground
are welcome in the KDE Games mailing list (kde-games-devel@kde.org).
Game-related news can be found on http://games.kde.org.

General KDE discussions should go to the KDE mailing list (kde@kde.org).


