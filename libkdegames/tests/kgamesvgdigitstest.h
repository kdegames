#ifndef KGAMESVGDIGITSTEST_H
#define KGAMESVGDIGITSTEST_H

#include "qtest_kde.h"
#include <KGameSvgDigits>

/// @brief A test class for KGameSvgDigits
class tst_KGameSvgDigits : public QObject
{
    Q_OBJECT

//     KGameSvgDigits m_svgDigits;

// Declare test functions as private slots, or they won't get executed
private slots:

    /// @brief This function is called first, so you can do init stuff here.
    void initTestCase();

    /// @brief This function is called last, so you can do final stuff here.
    void cleanupTestCase();
};

#endif // KGAMESVGDIGITSTEST_H
